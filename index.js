const getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`);

const address = ["143", "Atherton Street","San Mateo County", "California", '94027'];
const [houseNumber, street, city, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street} ${city}, ${state} ${zipCode}`);

const animal = {
	name: "Gigi Goode",
	species: "dog",
	breed: "Shih tzu",
	personality: "sweet",
	owner: "Ru Paul"
}

const {name, species, breed, personality, owner} = animal;
console.log(`${name} is a female ${breed}. She is a very ${personality} type of ${species} owned by ${owner}.`);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((x, y) => x + y);

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const myDog = new Dog("Gigi", 5, "Shih tzu");
console.log(myDog);